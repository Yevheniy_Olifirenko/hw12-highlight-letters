function changeColor() {
    const btns = document.querySelectorAll(".btn");

        document.addEventListener("keydown", event => {
            for (let btn of btns) {
                btn.classList.remove("active");
                if (btn.dataset.btn === event.key) {
                    btn.classList.add("active");
                };
            };
        });
}

changeColor();